<?php

use Illuminate\Support\Facades\Route;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (env('APP_ENV') == 'production' ||env('APP_ENV') == 'prod' )
{URL::forceScheme('https');}

Auth::routes();

Route::prefix('popup')->group(function () {
});

Route::get('/','LandingController@index');

Route::get('administrator','HomeController@index');
Route::prefix('admin')->group(function () {
	Route::prefix('dokumen')->group(function () {
		Route::get('/','Admin\DokumenController@index');
		Route::post('create','Admin\DokumenController@create');
		Route::post('save','Admin\DokumenController@save');
		Route::post('delete','Admin\DokumenController@delete');
		Route::get('datatables','Admin\DokumenController@datatables');
	});

	Route::prefix('gallery')->group(function () {
		Route::get('/','Admin\GalleryController@index');
		Route::post('create','Admin\GalleryController@create');
		Route::post('save','Admin\GalleryController@save');
		Route::post('cekupload','Admin\GalleryController@cekupload');
		Route::post('upload','Admin\GalleryController@upload');
		Route::post('delete','Admin\GalleryController@delete');
		Route::get('datatables','Admin\GalleryController@datatables');
	});

	Route::prefix('berita')->group(function () {
		Route::get('/','Admin\BeritaController@index');
		Route::post('create','Admin\BeritaController@create');
		Route::post('save','Admin\BeritaController@save');
		Route::post('cekupload','Admin\BeritaController@cekupload');
		Route::post('upload','Admin\BeritaController@upload');
		Route::post('delete','Admin\BeritaController@delete');
		Route::get('datatables','Admin\BeritaController@datatables');
	});

	Route::prefix('struktural')->group(function () {
		Route::get('/','Admin\StrukturalController@index');
		Route::post('create','Admin\StrukturalController@create');
		Route::post('save','Admin\StrukturalController@save');
		Route::post('delete','Admin\StrukturalController@delete');
		Route::get('datatables','Admin\StrukturalController@datatables');
	});
});

Route::prefix('log')->group(function () {

	Route::get('/', 'LogController@index');
	Route::get('datatables', 'LogController@datatables');
	Route::post('modal-new', 'LogController@modalNew');
	Route::post('modal-update', 'LogController@modalUpdate');
});


Route::prefix('page')->group(function(){
	Route::get('sejarah','PageController@sejarah');
	Route::get('program','PageController@program');
	Route::get('visi','PageController@visi');
	Route::get('tusi','PageController@tusi');
	Route::get('kepegawaian','PageController@kepegawaian');
	Route::get('organisasi','PageController@organisasi');
	Route::get('unit-kerja','PageController@unit_kerja');
	Route::get('strtuktral','PageController@strtuktral');
	Route::get('laporan','PageController@laporan');
	Route::get('laporan-datatable','PageController@laporan_datatable');
	Route::get('view_laporan/{id}','PageController@view_laporan');
	Route::get('dokumen','PageController@dokumen');
	Route::get('berita','PageController@berita');
	Route::get('berita/{id}','PageController@berita_detail');
	Route::get('gallery','PageController@gallery');
	Route::get('gallery/{id}','PageController@gallery_detail');
});

Route::prefix('download')->group(function(){
	Route::get('struktural/foto/{id}','DownloadController@foto_struktural');
	Route::get('berita/gambar/{id}','DownloadController@berita_gambar');
	Route::get('dokumen/{id}','DownloadController@download_dokumen');
});
Route::prefix('password')->group(function(){
	Route::post('update','Master\UserController@updatePassword');
});

Route::prefix('api')->group(function(){
});
