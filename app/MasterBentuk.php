<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MasterBentuk extends Model
{
    protected $table = 'master_bentuk_inovasi';
}
