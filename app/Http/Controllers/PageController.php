<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterJenisDokumen;
use App\Dokumen;
use Yajra\Datatables\Datatables;
use App\Berita;
use App\BeritaGambar;

class PageController extends Controller
{
    public function sejarah()
    {
    	return view('pages.landing.sejarah.index');
    }

    public function program()
    {
    	return view('pages.landing.program.index');
    }

    public function organisasi()
    {
    	return view('pages.landing.organisasi.index');
    }

    public function kepegawaian()
    {
    	return view('pages.landing.kepegawaian.index');
    }

    public function struktural()
    {
    	return view('pages.landing.struktural.index');
    }

    public function tusi()
    {
    	return view('pages.landing.tusi.index');
    }

    public function unit_kerja()
    {
    	return view('pages.landing.unit_kerja.index');
    }

    public function visi()
    {
    	return view('pages.landing.visi.index');
    }

    public function berita(){
    	$berita = Berita::where('id','>',0)->where('jenis','berita')->paginate(12);
    	return view('pages.landing.berita.index',compact('berita'));
    }

    public function berita_detail($id){
    	$berita = Berita::find($id);
    	return view('pages.landing.berita.detail',compact('berita'));
    }

    public function gallery(){
    	$berita = Berita::where('id','>',0)->where('jenis','gallery')->paginate(8);
    	return view('pages.landing.gallery.index',compact('berita'));
    }

    public function gallery_detail($id){
    	$berita = Berita::find($id);
    	return view('pages.landing.gallery.detail',compact('berita'));
    }

    public function laporan()
    {
    	return view('pages.landing.laporan.index');
    }

    public function laporan_datatable()
    {
    	$dokumen = Dokumen::where('id','>', 0)->where('jenis','2')->get();
        return Datatables::of($dokumen)
        ->addColumn('aksi',function($i){
        	return '<a type="button" target="_blank" href="'.url('page/view_laporan/'.$i->id).'" class="popover_edit btn btn-lg btn-primary">
                <i class="flaticon-open-archive"></i>
            </a>
            <a href="'.url('download/dokumen/'.$i->id).'" type="button" class="popover_delete btn btn-lg btn-primary" onclick="downloadDokumen('.$i->id.')">
              <i class="fa fa-download"></i> </a>';
        })->rawColumns(['aksi'])
        ->make(true);
    }

    public function view_laporan($id){
    	$dokumen = Dokumen::find($id);
    	return view('pages.landing.laporan.modal.pdf',compact('dokumen'));
    }
}
