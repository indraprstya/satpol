@extends('layouts.app_landing')
@section('content')

        <!-- Start Main Slider -->
        <section class="main-slider style4">

            <div class="slider-bg-slide"
                data-options='{ "delay": 5000, "slides": [ { "src": "assets/images/slides/slide-v4-1.jpg" }, { "src": "assets/images/slides/slide-v4-2.jpg" }, { "src": "assets/images/slides/slide-v4-3.jpg" } ], "transition": "slideDown", "animation": "kenburns", "timer": false, "align": "top" }'>
            </div><!-- /.banner-bg-slide -->
            <div class="slider-bg-slide-overly"></div>

            <div class="slider-box">
                <!-- Banner Carousel -->
                <div class="banner-carousel owl-theme owl-carousel">
                    <!-- Slide -->
                    <div class="slide">
                        <div class="auto-container">
                            <div class="content">
                                <!--Start Insurance Form Box-->
                                <!--End Insurance Form Box-->

                                <!--Start Slide Style4 Content Box-->
                                <div class="slide-style4-content-box">
                                    <div class="sub-title">
                                        <h3>Welcome To Our Website.</h3>
                                    </div>
                                    <div class="big-title">
                                        <h2>
                                            Insurance<br> <span>Advisory.</span>
                                        </h2>
                                    </div>
                                </div>
                                <!--End Slide Style4 Content Box-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- End Main Slider -->


        <!--Start About Style4 Area-->
        <section class="about-style4-area">
            <div class="container">
                <div class="row text-right-rtl">

                    <div class="col-xl-6">
                        <div class="about-style4__image clearfix">
                            <img src="assets/images/about/about-v4-img1.jpg" alt="" />
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="about-style4__content">
                            <div class="sec-title">
                                <h2>Tentang Kami
                                </h2>
                            </div>
                            <div class="inner-content">
                                <h2>Make business with us & Top Service.</h2>
                                <div class="text">
                                    <p>There are many variations of passages of Lorem Ipsum available, but majority have
                                        suffered alteration in some form, by injected humour, or randomised words which
                                        don't look even slightly believable.</p>
                                </div>

                                <ul>
                                    <li>
                                        <div class="icon">
                                            <span class="icon-right-arrow-2"></span>
                                        </div>

                                        <div class="text">
                                            <p>Our efforts are for the betterment of your business.Hard work is the key
                                                to improvement.</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="icon">
                                            <span class="icon-right-arrow-2"></span>
                                        </div>

                                        <div class="text">
                                            <p>There are many variations of passages of Lorem Ipsum available, but the
                                                majority have form.</p>
                                        </div>
                                    </li>
                                </ul>

                                <div class="btns-box">
                                    <a class="btn-one" href="project.html">
                                        <span class="border-box"></span>
                                        <span class="bg bg--white"></span>
                                        <span class="txt">Selengkapnya<i class="icon-left-arrow arrow"></i>
                                        </span>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--End About Style4 Area-->

        <!--Start Fact Counter Area-->
        <section class="fact-counter-area">
            <div class="container">
                <div class="row">
                    <!--Start Single Fact Counter-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-fact-counter text-center">
                            <div class="inner">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="3000" data-stop="4.6">0</span>
                                </div>
                            </div>
                            <div class="title">
                                <h3>33+ Branch avg rating</h3>
                                <div class="review-box">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Fact Counter-->

                    <!--Start Single Fact Counter-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-fact-counter text-center">
                            <div class="inner">
                                <div class="count-outer count-box">
                                    <span class="count-text" data-speed="3000" data-stop="230">0</span>
                                    <span class="icon-plus-sign plus"></span>
                                </div>
                            </div>
                            <div class="title">
                                <h3>Customer reviews</h3>
                                <div class="review-box">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Fact Counter-->

                    <!--Start Single Fact Counter-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-fact-counter text-center">
                            <div class="inner">
                                <div class="count-outer count-box">
                                    <span>24/7</span>
                                </div>
                            </div>
                            <div class="title">
                                <h3>100% Support rating</h3>
                                <div class="review-box">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Fact Counter-->
                </div>
            </div>
        </section>
        <!--End Fact Counter Area-->


        <!--Start Service Style3 Area-->
        <section class="service-style3-area">
            <div class="container">
                <div class="sec-title-style2 style2instyle3 text-center">
                    <div class="icon">
                        <span class="icon-picasa-logo"></span>
                    </div>
                    <h2><span>Gallery Foto</span> </h2>
                </div>
                <div class="row text-right-rtl">
                    <!--Start Single Service Style3-->
                    <div class="col-xl-4 col-lg-4 col-md-12 wow fadeInUp" data-wow-delay="00ms"
                        data-wow-duration="1500ms">
                        <div class="single-service-style3">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/services/service-v3-1.jpg" alt="">
                                    <div class="icon">
                                        <span class="icon-car-1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <h3><a href="service-details.html"><span>Car</span> Insurance.</a></h3>
                                <p>Donec dapibus velit at lacus varius aptent finibus. Class aptent taciti.</p>
                            </div>
                        </div>
                    </div>
                    <!--End Single Service Style3-->
                    <!--Start Single Service Style3-->
                    <div class="col-xl-4 col-lg-4 col-md-12 wow fadeInUp" data-wow-delay="00ms"
                        data-wow-duration="1500ms">
                        <div class="single-service-style3">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/services/service-v3-2.jpg" alt="">
                                    <div class="icon">
                                        <span class="icon-life-insurance-1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <h3><a href="service-details.html"><span>Life</span> Insurance.</a></h3>
                                <p>Donec dapibus velit at lacus varius aptent finibus. Class aptent taciti.</p>
                            </div>
                        </div>
                    </div>
                    <!--End Single Service Style3-->
                    <!--Start Single Service Style3-->
                    <div class="col-xl-4 col-lg-4 col-md-12 wow fadeInUp" data-wow-delay="00ms"
                        data-wow-duration="1500ms">
                        <div class="single-service-style3">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/services/service-v3-3.jpg" alt="">
                                    <div class="icon">
                                        <span class="icon-safe"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <h3><a href="service-details.html"><span>Health</span> Insurance.</a></h3>
                                <p>Donec dapibus velit at lacus varius aptent finibus. Class aptent taciti.</p>
                            </div>
                        </div>
                    </div>
                    <!--End Single Service Style3-->

                </div>
            </div>
        </section>
        <!--End Service Style3 Area-->

        <!--Start Slogan Style2 Area-->
        <section class="slogan-style2-area slogan-style2--instyle3-area">
            <div class="auto-container">
                <div class="slogan-style2__bg"
                    style="background-image: url(assets/images/parallax-background/slogan-bg-3.jpg);"></div>
                <div class="slogan-style2__content text-center">
                    <h2>Like our service? Subscribe us.</h2>
                    <div class="slogan-style2-btn">
                        <a class="btn-one style2" href="contact.html">
                            <span class="border-box"></span>
                            <span class="bg bg--gradient"></span>
                            <span class="txt">Free Quate<i class="icon-left-arrow arrow"></i></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--Start Slogan Style2 Area-->

        <!--End Testimonail Style3 Area-->

        <!--Start Awards Area-->
        <!--End Awards Area-->


        <!--Start Team Style2 Area-->
        <section class="team-style2-area">
            <div class="container">
                <div class="sec-title-style2 style2instyle3 text-center">
                    <div class="icon">
                        <span class="icon-picasa-logo"></span>
                    </div>
                    <h2><span>Kegiatan Satuan Polisi Pamong Praja Provinsi Jawa Timur</span></h2>
                </div>
                <div class="row">
                    <!--Start Single Team Style1-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-team-style1">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/team/team-v2-1.jpg" alt="" />
                                    <div class="social-link-box-style2">
                                        <div class="inner-box">
                                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <div class="name">
                                    <h3><a href="team-single.html">Kutub <span>Al Mahi</span></a></h3>
                                    <div class="bottom">
                                        <img src="assets/images/icon/thm-logo-2.png" alt="">
                                        <p>CEO & Co Founder</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Team Style1-->
                    <!--Start Single Team Style1-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-team-style1">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/team/team-v2-2.jpg" alt="" />
                                    <div class="social-link-box-style2">
                                        <div class="inner-box">
                                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <div class="name">
                                    <h3><a href="team-single.html">Kamrul <span>Islam Mona</span></a></h3>
                                    <div class="bottom">
                                        <img src="assets/images/icon/thm-logo-2.png" alt="">
                                        <p>CEO & Co Founder</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Team Style1-->
                    <!--Start Single Team Style1-->
                    <div class="col-xl-4 col-lg-4">
                        <div class="single-team-style1">
                            <div class="img-holder">
                                <div class="inner">
                                    <img src="assets/images/team/team-v2-3.jpg" alt="" />
                                    <div class="social-link-box-style2">
                                        <div class="inner-box">
                                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="title-holder">
                                <div class="name">
                                    <h3><a href="team-single.html">Raihan <span>Rafi Mira</span></a></h3>
                                    <div class="bottom">
                                        <img src="assets/images/icon/thm-logo-2.png" alt="">
                                        <p>CEO & Co Founder</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Single Team Style1-->
                </div>
            </div>
        </section>
        <!--End Team Style2 Area-->

        <!--Start Partner Style2 Area-->
        <section class="partner-style2-area">
            <div class="container">
                <div class="sec-title-style2 style2instyle3 text-center">
                    <div class="icon">
                        <span class="icon-picasa-logo"></span>
                    </div>
                    <h2><span>Situs Terkait</span></h2>
                </div>
                <div class="inner">
                    <ul class="partner-box partner-carousel-2 owl-carousel owl-theme owl-dot-style1 rtl-carousel">
                        <!--Start Single Partner Logo Box-->
                        <li class="single-partner-logo-box" style="background-color: white;">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/jatimprov.jpg') }}" alt="Awesome Image"></a>
                        </li>
                        <!--End Single Partner Logo Box-->
                        <!--Start Single Partner Logo Box-->
                        <li class="single-partner-logo-box" style="background-color: white;">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/lapor.jpg') }}" alt="Awesome Image"></a>
                        </li>
                        <!--End Single Partner Logo Box-->
                        <!--Start Single Partner Logo Box-->
                        <li class="single-partner-logo-box" style="background-color: white;">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/sibekisar.jpg') }}" alt="Awesome Image"></a>
                        </li>
                        <!--End Single Partner Logo Box-->
                        <!--Start Single Partner Logo Box-->
                        <li class="single-partner-logo-box"  style="background-color: white;">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/bakorwil_bojonegoro.png') }}" alt="Awesome Image"></a>
                        </li>
                        <!--End Single Partner Logo Box-->
                        <!--Start Single Partner Logo Box-->
                        <li class="single-partner-logo-box">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/bakorwil_malang.png') }}" alt="Awesome Image"></a>
                        </li>
                        <li class="single-partner-logo-box">
                            <a href="#"><img src="{{ asset('assets/landing/images/icon/bakorwil_madiun.png') }}" alt="Awesome Image"></a>
                        </li>
                        <!--End Single Partner Logo Box-->
                    </ul>
                </div>
            </div>
        </section>
        <!--End Partner Style2 Area-->



@endsection
