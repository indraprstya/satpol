import Alpine from 'alpinejs'
window.Alpine = Alpine;
import Tooltip from "@ryangjchandler/alpine-tooltip";
Alpine.plugin(Tooltip);
Alpine.start()
